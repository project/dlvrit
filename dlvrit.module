<?php

/**
 * @file
 * API integration module file for dlvr.it service.
 */

use Html2Text\Html2Text;

define('DLVRIT_FIELD_SETTINGS_FORM', 'settings');
define('DLVRIT_FIELD_TEXT_WIDGET_FORM', 'text_widget');
define('DLVRIT_FIELD_IMAGE_WIDGET_FORM', 'image_widget');

/**
 * Implements hook_menu().
 */
function dlvrit_menu() {
  $items['admin/config/services/dlvrit'] = array(
    'title' => 'Social media posts',
    'description' => 'Settings for dlvr.it service',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('dlvrit_global_settings_form'),
    'access arguments' => array('manage dlvrit settings'),
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function dlvrit_permission() {
  return array(
    'share dlvrit posts' => array(
      'title' => t('Share social media posts.'),
      'description' => t('Share social media posts using dlvr.it API.'),
    ),
    'manage dlvrit settings' => array(
      'title' => t('Manage dvlr.it API settings.'),
      'description' => t('Manage dlvr.it configuration.'),
    ),
  );
}

/**
 * Dlvr.it API client settings form.
 */
function dlvrit_global_settings_form($form, &$form_state) {
  $form['dlvrit_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('dlvr.it API key'),
    '#description' => t('May be obtained at account settings page.'),
    '#default_value' => variable_get('dlvrit_api_key', ''),
    '#required' => TRUE,
  );

  $form['dlvrit_url_shortener'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use dlvr.it URL shortener service'),
    '#description' => t('<b>Note</b>: Requires paid plan.'),
    '#default_value' => variable_get('dlvrit_url_shortener', FALSE),
  );

  $form['dlvrit_image_style'] = array(
    '#type' => 'select',
    '#title' => t('Image style'),
    '#description' => t('Image style to use for processing social networks images.'),
    '#default_value' => variable_get('dlvrit_image_style', 'dlvrit_image'),
    '#options' => image_style_options(TRUE, PASS_THROUGH),
  );

  $form['dlvrit_prevent_sharing_unpublished_nodes'] = array(
    '#type' => 'checkbox',
    '#title' => t('Prevent sharing unpublished nodes'),
    '#default_value' => variable_get('dlvrit_prevent_sharing_unpublished_nodes', TRUE),
  );

  $form = system_settings_form($form);
  $form['#submit'][] = 'dlvrit_flush_cache';

  return $form;
}

/**
 * Dlvr.it global settings form validate callback.
 */
function dlvrit_global_settings_form_validate($form, &$form_state) {
  try {
    // Try running simple API request and see if it fails.
    dlvrit_api_client($form_state['values']['dlvrit_api_key'])
      ->getAccounts();
  }
  catch (DlvrItApiException $e) {
    form_set_error('dlvrit_api_key', t('Error communicating dlvr.it API. Check logs for details.'));
    watchdog_exception('dlvrit', $e);
  }
}

/**
 * Flush module caches.
 */
function dlvrit_flush_cache() {
  drupal_static_reset('dlvrit_social_accounts');
  cache_clear_all('dlvrit:*', 'cache', TRUE);
}

/**
 * List of text field types which may be used for posting to social networks.
 */
function dlvrit_text_field_types() {
  return array(
    'text',
    'text_long',
    'text_with_summary',
  );
}

/**
 * List of field types which may be used for posting media.
 */
function dlvrit_media_field_types() {
  return array(
    'image',
  );
}

/**
 * Add dlvr.it settings to field settings form.
 *
 * Implements hook_form_FORM_ID_alter().
 */
function dlvrit_form_field_ui_field_edit_form_alter(&$form, &$form_state, $form_id) {
  $is_text_field = in_array($form['#field']['type'], dlvrit_text_field_types());
  $is_media_field = in_array($form['#field']['type'], dlvrit_media_field_types());

  if (!($is_text_field || $is_media_field)) {
    // Only apply to text and media fields.
    return;
  }

  try {
    $accounts = dlvrit_social_accounts();
  }
  catch (DlvrItApiException $e) {
    watchdog_exception('dlvrit', $e);
    $form['instance']['settings']['dlvrit_error']['#markup']
      = t("An error occured while communicating with dlvr.it API, so the social networks settings aren't available. Consider checking system logs.");

    // Preserve saved values.
    $form['instance']['settings']['dlvrit_enabled'] = array(
      '#type' => 'value',
      '#value' => !empty($form['#instance']['settings']['dlvrit_enabled']),
    );

    $form['instance']['settings']['dlvrit_accounts'] = array(
      '#type' => 'value',
      '#value' => !empty($form['#instance']['settings']['dlvrit_accounts']) ? $form['#instance']['settings']['dlvrit_accounts'] : array(),
    );
    return;
  }

  $form['instance']['settings']['dlvrit_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable posting to social networks'),
    '#default_value' => !empty($form['#instance']['settings']['dlvrit_enabled']),
  );

  if ($is_text_field) {
    $form['instance']['settings']['dlvrit_always_link_post'] = array(
      '#type' => 'checkbox',
      '#title' => t('Always link post to content'),
      '#description' => t('Enabling this option will hide "Link post to this content" checkbox for this field at content edit page.'),
      '#default_value' => !empty($form['#instance']['settings']['dlvrit_always_link_post']),
      '#states' => array(
        'visible' => array(
          ':input[name="instance[settings][dlvrit_enabled]"]' => array('checked' => TRUE),
        ),
      ),
    );
  }
  elseif ($is_media_field) {
    $form['instance']['settings']['dlvrit_always_attach'] = array(
      '#type' => 'checkbox',
      '#title' => t('Always attach'),
      '#description' => t('Enabling this option will hide accounts selection for this field at content edit page.'),
      '#default_value' => !empty($form['#instance']['settings']['dlvrit_always_attach']),
      '#states' => array(
        'visible' => array(
          ':input[name="instance[settings][dlvrit_enabled]"]' => array('checked' => TRUE),
        ),
      ),
    );
  }

  $form['instance']['settings']['dlvrit_accounts'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Social accounts'),
    '#description' => t('Please select social networks accounts which should be used for posting. To refresh accounts links, try re-saving dlvr.it !settings form.', array('!settings' => l(t('settings'), 'admin/config/services/dlvrit'))),
    '#multiple' => TRUE,
    '#options' => dlvrit_accounts_form_options($accounts, DLVRIT_FIELD_SETTINGS_FORM),
    '#default_value' => !empty($form['#instance']['settings']['dlvrit_accounts']) ? $form['#instance']['settings']['dlvrit_accounts'] : array(),
    '#states' => array(
      'visible' => array(
        ':input[name="instance[settings][dlvrit_enabled]"]' => array('checked' => TRUE),
      ),
    ),
    '#element_validate' => array(
      'dlvrit_form_field_ui_field_edit_form_accounts_validation',
    ),
  );

  if ($unavailable_accounts = dlvrit_unavailable_accounts($accounts)) {
    $list = &$form['instance']['settings']['dlvrit_unavailable_accounts'];

    $list = array(
      '#theme' => 'item_list',
      '#title' => t("The following social media accounts aren't available for usage:"),
      '#items' => array(),
    );
    foreach ($unavailable_accounts as $account) {
      $list['#items'][] = $account['service'] . ' - <em>' . $account['name'] . '</em>: ' . $account['error'];
    }
  }

  // Helper alter hook.
  drupal_alter('dlvrit_field_ui_form', $form, $accounts);
}

/**
 * Get list of dlvr.it accounts for using in form element '#options'.
 *
 * @param array $accounts
 *   List of accounts, as returned by dlvr.it API.
 * @param string $form_type
 *   Form type, one of DLVRIT_FIELD_*_FORM constants.
 *
 * @return array
 *   List of account names, keyed by account ID.
 */
function dlvrit_accounts_form_options(array $accounts, $form_type = DLVRIT_FIELD_SETTINGS_FORM) {
  $accounts_options = array();
  foreach ($accounts as $id => $account) {
    if (!empty($account['error'])) {
      // Skip account if it's not available.
      continue;
    }
    switch ($form_type) {
      case DLVRIT_FIELD_TEXT_WIDGET_FORM:
        // @TODO: Append account name if there are more than one account for the
        // @TODO: same site, e.g. two Twitter accounts.
        $title = t('Post to @service', array('@service' => $account['service']));
        break;

      case DLVRIT_FIELD_IMAGE_WIDGET_FORM:
        // @TODO: Append account name if there are more than one account for the
        // @TODO: same site, e.g. two Twitter accounts.
        $title = t('Attach to @service post', array('@service' => $account['service']));
        break;

      case DLVRIT_FIELD_SETTINGS_FORM:
      default:
        $title = $account['service'] . ' - <em>' . $account['name'] . '</em>';
        break;
    }

    $accounts_options[$id] = $title;
  }
  return $accounts_options;
}

/**
 * Form validation callback for social networks accounts selection at field UI.
 *
 * @see dlvrit_form_field_ui_field_edit_form_alter()
 */
function dlvrit_form_field_ui_field_edit_form_accounts_validation(&$element, &$form_state) {
  if (empty($form_state['values']['instance']['settings']['dlvrit_enabled'])) {
    // Skip if posting to social media is disabled.
    return;
  }

  $accounts = array_filter($form_state['values']['instance']['settings']['dlvrit_accounts']);
  if (empty($accounts)) {
    form_error($element, t('Please either disable posting to social networks or select at least one social network account.'));
  }
}

/**
 * Implements hook_field_widget_form_alter().
 */
function dlvrit_field_widget_form_alter(&$element, &$form_state, $context) {
  $is_text_field = in_array($context['field']['type'], dlvrit_text_field_types());
  $is_media_field = in_array($context['field']['type'], dlvrit_media_field_types());

  if (($is_text_field || $is_media_field)
    && !empty($context['instance']['settings']['dlvrit_enabled'])) {
    // Field instance setting is always available.
    try {
      $accounts = array_intersect_key(dlvrit_social_accounts(), array_filter($context['instance']['settings']['dlvrit_accounts']));
      if (!$accounts) {
        // Do nothing if there are no accounts available.
        return;
      }

      if (field_behaviors_widget('multiple values', $context['instance']) == FIELD_BEHAVIOR_CUSTOM) {
        $widgets = &$element;
      }
      else {
        $widgets = array(&$element);
      }

      foreach ($widgets as &$widget) {
        $widget['dlvrit'] = array(
          '#type' => 'container',
          '#weight' => 10,
        );

        if ($is_text_field) {
          // Text field.
          $widget['dlvrit']['dlvrit_post_accounts'] = array(
            '#type' => 'checkboxes',
            // Nothing selected.
            '#default_value' => array_fill_keys(array_keys($accounts), 0),
            '#options' => dlvrit_accounts_form_options($accounts, DLVRIT_FIELD_TEXT_WIDGET_FORM),
          );
          if ($context['instance']['entity_type'] == 'node') {
            $widget['dlvrit']['dlvrit_post_accounts']['#element_validate'] = array('dlvrit_post_accounts_validate');
          }
        }
        elseif ($is_media_field) {
          // Media field. Only display accounts selection if 'Always attach'.
          if (empty($context['instance']['settings']['dlvrit_always_attach'])) {
            $widget['dlvrit']['dlvrit_post_accounts'] = array(
              '#type' => 'checkboxes',
              // All checkboxes are selected by default.
              '#default_value' => array_keys($accounts),
              '#options' => dlvrit_accounts_form_options($accounts, DLVRIT_FIELD_IMAGE_WIDGET_FORM),
            );
          }
        }

        if ($is_text_field
          && empty($context['instance']['settings']['dlvrit_always_link_post'])) {
          // Only show 'Link post to this content' checkbox text fields if
          // there's no 'Always link post to content' set.
          $widget['dlvrit']['dlvrit_append_url'] = array(
            '#type' => 'checkbox',
            '#title' => t('Link post to this content'),
            '#default_value' => TRUE,
          );
        }
      }
    }
    catch (DlvrItApiException $e) {
      $element['dlvrit']['#markup']
        = t("An error occured while communicating with dlvr.it API, so the social networks settings aren't available. Consider checking system logs.");
      watchdog_exception('dlvrit', $e);
      return;
    }
  }
}

/**
 * Validation function for dlvrit_post_accounts fields.
 */
function dlvrit_post_accounts_validate($element, $form_state) {
  $accounts = array_filter($element['#value']);
  if (!variable_get('dlvrit_prevent_sharing_unpublished_nodes', TRUE)) {
    return;
  }
  if (empty($accounts)) {
    return;
  }
  if (empty($form_state['values']['status'])) {
    form_error($element, t('You can not share unpublished content.'));
  }
}

/**
 * Implements hook_entity_insert().
 */
function dlvrit_entity_insert($entity, $type) {
  dlvrit_process_entity($type, $entity);
}

/**
 * Implements hook_entity_update().
 */
function dlvrit_entity_update($entity, $type) {
  dlvrit_process_entity($type, $entity);
}

/**
 * Get list of social accounts.
 */
function dlvrit_social_accounts($reset = FALSE) {
  $accounts = &drupal_static(__FUNCTION__);

  if (!isset($accounts) || $reset) {
    $cid = 'dlvrit:social_accounts';

    if (!$reset
      && $cache = cache_get($cid)) {
      $accounts = $cache->data;
    }
    else {
      $accounts = array();

      foreach ($response = dlvrit_api_client()
        ->getAccounts() as $delta => $account) {
        $id = $account['id'];

        if ($account['error'] = dlvrit_account_error($account)) {
          $id = 'error_' . $delta;
        }

        $accounts[$id] = $account;
      }

      cache_set($cid, $accounts);
    }
  }

  return $accounts;
}

/**
 * Get API client object.
 *
 * @param string|null $api_key
 *   API key or NULL to use global settings.
 *
 * @return DlvrItApiClient
 *   API client object.
 */
function dlvrit_api_client($api_key = NULL) {
  return DlvrItApiClient::singleton($api_key);
}

/**
 * Process entity fields and share post in social networks.
 *
 * @param string $entity_type
 *   Entity type.
 * @param object $entity
 *   Entity object.
 */
function dlvrit_process_entity($entity_type, $entity) {
  list($entity_id, $revision_id, $bundle) = entity_extract_ids($entity_type, $entity);
  foreach (field_info_instances($entity_type, $bundle) as $field_instance) {
    if (empty($field_instance['settings']['dlvrit_enabled'])) {
      // Skip if posting to social networks is disabled.
      continue;
    }
    $field_name = $field_instance['field_name'];

    // Now check if posting is possible; there's a possible scenario when
    // posting was enabled in field settings but the field type was excluded
    // later.
    $field_info = field_info_field($field_name);
    if (!isset($field_info['type'])
      || !in_array($field_info['type'], dlvrit_text_field_types())) {
      // Skip if posting is not possible for a given field type.
      continue;
    }

    if (!($field_items = field_get_items($entity_type, $entity, $field_name))) {
      // Skip if field is empty.
      continue;
    }
    /* @var array $field_items */

    foreach ($field_items as $delta => $field_item) {
      if (empty($field_item['dlvrit']['dlvrit_post_accounts'])) {
        // Skip field item if there are no accounts options.
        continue;
      }

      $selected_accounts = array_keys(array_filter($field_item['dlvrit']['dlvrit_post_accounts']));
      if (!$selected_accounts) {
        // Skip field item if there are no accounts selected.
        continue;
      }

      $url = NULL;

      // Generate content URL.
      if ((!empty($field_item['dlvrit']['dlvrit_append_url'])
          || !empty($field_instance['settings']['dlvrit_always_link_post']))
        && $entity_uri = entity_uri($entity_type, $entity)) {
        $url = url($entity_uri['path'], $entity_uri['options'] + array('absolute' => TRUE));
        if (variable_get('dlvrit_url_shortener', FALSE)) {
          try {
            $url = dlvrit_api_client()
              ->shortenUrl($url);
          }
          catch (DlvrItApiResponseException $e) {
            if ($e->getCode() == 107) {
              drupal_set_message(t('Error shortening content URL; paid dlvr.it plan required.'), 'error');
            }
            else {
              drupal_set_message(t('An error occurred while shortening content URL. Check logs for details.', 'error'));
              watchdog_exception('dlvrit', $e);
            }
          }
        }
      }

      $text = dlvrit_prepare_text($field_item['value'], $url);

      foreach ($selected_accounts as $account) {
        // @TODO: Do not post immediately, use queue + shutdown hook similar to
        // @TODO: what Search API does.
        $image_path = NULL;
        $image_mime = NULL;
        if ($image_file = dlvrit_find_image_to_post($entity_type, $entity, $account)) {
          if ($path_and_mime = dlvrit_prepare_image($image_file)) {
            list ($image_path, $image_mime) = $path_and_mime;
          }
        }
        try {
          dlvrit_api_client()
            ->postToAccount($account, $text, $image_path, $image_mime);
        }
        catch (DlvrItApiException $e) {
          drupal_set_message(t('An error occurred while sharing your post in social networks. Check logs for details.', 'error'));
          watchdog_exception('dlvrit', $e);
        }
      }
    }
  }
}

/**
 * Prepare text for posting.
 *
 * The method strips out HTML, trims whitespaces and appends content URL.
 *
 * @param string $text
 *   Original context text.
 * @param string|null $url
 *   Content URL or NULL.
 *
 * @return string
 *   Post text prepared for posting.
 */
function dlvrit_prepare_text($text, $url) {
  // Check if the Html2Text library is installed and strip HTML.
  if (method_exists('\Html2Text\Html2Text', 'getText')) {
    $text = (new Html2Text($text, array('width' => 0)))->getText();
  }
  else {
    watchdog('dlvrit', 'dlvrit initialization failed: Unable to load the Html2Text library.', NULL, WATCHDOG_ERROR);
  }

  // Replace newlines.
  $text = str_replace("\r\n", "\n", $text);
  $text = preg_replace('/(\n){2,}/', "\n", $text);

  // Trim whitespaces.
  $text = trim($text);

  // Append URL.
  if ($url) {
    $text .= PHP_EOL . $url;
  }

  return $text;
}

/**
 * Find image for posting.
 *
 * The function loops through all entity media fields (sorted by weight) and
 * finds the first suitable value.
 *
 * @param string $entity_type
 *   Entity type.
 * @param object $entity
 *   Entity object.
 * @param int|null $account
 *   dlvr.it account ID or NULL to skip account check.
 *
 * @return object|null
 *   Image file object or NULL.
 */
function dlvrit_find_image_to_post($entity_type, $entity, $account = NULL) {
  list($entity_id, $revision_id, $bundle) = entity_extract_ids($entity_type, $entity);
  foreach (dlvrit_get_media_fields_instances($entity_type, $bundle) as $field_instance) {
    $field_name = $field_instance['field_name'];
    if ($field_items = field_get_items($entity_type, $entity, $field_name)) {
      /* @var array $field_items */
      foreach ($field_items as $field_item) {
        // Image is usable if one of the following conditions is met:
        // a) $account is NULL.
        // b) 'Always attach' option is enabled at field settings.
        // c) Account checkbox is enabled at field widget.
        if ($account === NULL
          || !empty($field_instance['settings']['dlvrit_always_attach'])
          || (!empty($field_item['dlvrit']['dlvrit_post_accounts'])
            && in_array($account, array_filter($field_item['dlvrit']['dlvrit_post_accounts'])))) {
          return file_load($field_item['fid']);
        }
      }
    }
  }
}

/**
 * Get media fields instances for given entity type and bundle.
 *
 * The output is sorted by field weight.
 *
 * @param string $entity_type
 *   Entity type.
 * @param string $bundle
 *   Bundle.
 *
 * @return array
 *   List of media fields instances.
 */
function dlvrit_get_media_fields_instances($entity_type, $bundle) {
  // @TODO: Add static cache?
  $instances = array();

  foreach (field_info_instances($entity_type, $bundle) as $field_instance) {
    if (empty($field_instance['settings']['dlvrit_enabled'])) {
      // Skip if posting to social networks is disabled.
      continue;
    }
    $field_name = $field_instance['field_name'];

    // Now check if posting is possible; there's a possible scenario when
    // posting was enabled in field settings but the field type was excluded
    // later.
    $field_info = field_info_field($field_name);

    if (in_array($field_info['type'], dlvrit_media_field_types())) {
      $instances[] = $field_instance;
    }
  }

  uasort($instances, 'dlvrit_sort_widget_weight');

  return $instances;
}

/**
 * Sort field instances by widget weight.
 *
 * @see drupal_sort_weight()
 */
function dlvrit_sort_widget_weight($a, $b) {
  $a_weight = (is_array($a) && isset($a['widget']['weight'])) ? $a['widget']['weight'] : 0;
  $b_weight = (is_array($b) && isset($b['widget']['weight'])) ? $b['widget']['weight'] : 0;
  if ($a_weight == $b_weight) {
    return 0;
  }
  return ($a_weight < $b_weight) ? -1 : 1;
}

/**
 * Implements hook_image_default_styles().
 */
function dlvrit_image_default_styles() {
  $styles = array();

  $styles['dlvrit_image'] = array(
    'label' => 'Social networks image',
    'effects' => array(
      array(
        'name' => 'image_scale',
        'data' => array('width' => 1500, 'height' => NULL, 'upscale' => 0),
        'weight' => '1',
      ),
    ),
  );

  return $styles;
}

/**
 * Prepare image for uploading to dlvr.it API.
 *
 * @param object $file
 *   Image file object.
 *
 * @return array|null
 *   Image path and MIME type or NULL.
 */
function dlvrit_prepare_image($file) {
  $original = array(
    $file->uri,
    $file->filemime,
  );

  // No image style selected.
  if (!($style_name = variable_get('dlvrit_image_style', 'dlvrit_image'))) {
    return $original;
  }

  // Invalid image style.
  if (!($style = image_style_load($style_name))) {
    watchdog('dlvrit', 'Error loading image style %name, original image will be uploaded.', array('%name' => $style_name), WATCHDOG_ERROR);
    return $original;
  }

  $temporary_uri = 'temporary://dlvrit-image/' . $style['name'] . '/' . $file->fid . '.jpg';

  // Handle image style generation failure.
  if (!file_exists($temporary_uri)
    && !image_style_create_derivative($style, $file->uri, $temporary_uri)) {
    watchdog('dlvrit', 'Error saving image style %name for file %fid, original image will be uploaded.', array('%name' => $style_name, '%fid' => $file->fid), WATCHDOG_ERROR);
    return $original;
  }

  // Return image.
  return array(
    $temporary_uri,
    'image/jpeg',
  );
}

/**
 * Check if there're any problem using the account.
 *
 * @param array $account
 *   dlvr.it account array, as returned by API.
 *
 * @return string|null
 *   Error string if account is not available or NULL.
 */
function dlvrit_account_error(array $account) {
  if (strtolower($account['service']) == 'gplus'
    && !is_numeric($account['id'])) {
    // Skip Google+ accounts if posting via API is not available.
    // @see https://support.dlvrit.com/hc/en-us/articles/200403134-Posting-to-Google-Pages-and-Profiles-now-supported-via-dlvr-it-posting-API
    // @see https://www.drupal.org/node/2922388
    return t('Using Google+ account via API is forbidden. Details are available at the !link.', array(
      '!link' => l(t('dlvr.it help topic'), 'https://support.dlvrit.com/hc/en-us/articles/200403134-Posting-to-Google-Pages-and-Profiles-now-supported-via-dlvr-it-posting-API'),
    ));
  }
}

/**
 * Get list of social networks accounts which aren't available for usage.
 *
 * As for the writing, the only known case is using Google+ via API.
 *
 * @param array $accounts
 *   List of accounts, as returned by dlvr.it API.
 *
 * @return array
 *   List of accounts which aren't available for usage.
 *
 * @see dlvrit_account_error()
 */
function dlvrit_unavailable_accounts(array $accounts) {
  $unavailable_accounts = array();

  foreach ($accounts as $account) {
    // Filled by dlvrit_account_error().
    if (!empty($account['error'])) {
      $unavailable_accounts[] = $account;
    }
  }

  return $unavailable_accounts;
}
