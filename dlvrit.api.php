<?php

/**
 * @file
 * Dlvr.it integration hooks.
 */

/**
 * Field settings UI helper hook.
 *
 * This hook may be used to provide additional settings for dlvr.it powered
 * fields.
 *
 * @param array $form
 *   Field settings form.
 * @param array $accounts
 *   Array of dlvr.it accounts.
 *
 * @see dlvrit_form_field_ui_field_edit_form_alter()
 */
function hook_dlvrit_field_ui_form_alter(array &$form, array $accounts) {
  // Helper variables.
  $is_text_field = in_array($form['#field']['type'], dlvrit_text_field_types());
  $is_media_field = in_array($form['#field']['type'], dlvrit_media_field_types());

  if ($is_media_field) {
    $form['instance']['settings']['dlvrit_MY_SETTING'] = array(
      '#type' => 'checkbox',
      '#title' => t('My custom setting for dlvr.it powered field'),
      '#description' => t('Example setting.'),
      '#default_value' => !empty($form['#instance']['settings']['dlvrit_MY_SETTING']),
    );
  }
}
