<?php

/**
 * @file
 * Dlvr.it API client implementation.
 */

/**
 * Dlvr.it API client implementation.
 *
 * The implementation relies on JSON API as documented.
 *
 * @see https://api.dlvrit.com/1/help
 */
class DlvrItApiClient {

  protected static $singleton = NULL;

  /**
   * API key override.
   *
   * @var string
   */
  protected $apiKey;

  /**
   * API client singleton instance.
   *
   * @param string|null $api_key
   *   API key or NULL to use global settings.
   *
   * @return DlvrItApiClient
   *   Client object.
   */
  public static function singleton($api_key = NULL) {
    $array_key = 'default';
    if ($api_key !== NULL) {
      $array_key = $api_key;
    }
    $class_name = get_called_class();
    if (!isset(static::$singleton[$class_name][$array_key])) {
      static::$singleton[$class_name][$array_key] = new $class_name($api_key);
    }
    return static::$singleton[$class_name][$array_key];
  }

  /**
   * API client constructor.
   *
   * @param string|null $api_key
   *   API key or NULL to use global settings.
   */
  public function __construct($api_key = NULL) {
    $this->setApiKey($api_key);
  }

  /**
   * Get list of social networks accounts.
   *
   * Refer to 'List Accounts' section.
   *
   * @see https://api.dlvrit.com/1/help
   */
  public function getAccounts() {
    $response = $this->performApiRequest('accounts');

    if (!isset($response['status'])) {
      throw new DlvrItApiResponseException('Missing response "status" property.');
    }

    if ($response['status'] != 'ok') {
      if (!empty($response['errors'])) {
        $error = key($response['errors']);
        throw new DlvrItApiResponseException('', $error);
      }
      else {
        throw new DlvrItApiResponseException('Response status is failure, but no error code supplied.');
      }
    }

    if (!isset($response['accounts'])) {
      throw new DlvrItApiResponseException('Missing response "accounts" property.');
    }

    return $response['accounts'];
  }

  /**
   * Post to social network using given account.
   *
   * @param int $account
   *   dlvr.it social account ID.
   * @param string $text
   *   Text to post.
   * @param string|null $image_path
   *   Image path.
   * @param string $image_mime_type
   *   Image MIME content type.
   *
   * @throws DlvrItApiResponseException
   *   Error sharing post.
   */
  public function postToAccount($account, $text, $image_path = NULL, $image_mime_type = NULL) {
    $params = array(
      'id' => $account,
      'msg' => $text,
    );
    if ($image_path && file_exists($image_path)) {
      $params['media'] = (object) array(
        'type' => 'file',
        'path' => $image_path,
        'mime_type' => $image_mime_type,
      );
    }
    $response = $this->performApiRequest('postToAccount', $params);

    if (!isset($response['id'])) {
      throw new DlvrItApiResponseException('Missing API response message ID.');
    }

    if (!isset($response['msg']) || $response['msg'] != 'posted') {
      throw new DlvrItApiResponseException('Unexpected API response: @response.', array('@response' => var_export($response, TRUE)));
    }
  }

  /**
   * Require short URL using dlvr.it URL shortening service.
   *
   * @param string $url
   *   Absolute URL.
   *
   * @return string
   *   Shorten URL.
   *
   * @throws DlvrItApiResponseShortenerPlanException
   *   Missing URL shortener support.
   * @throws DlvrItApiResponseException
   *   API response error.
   */
  public function shortenUrl($url) {
    $response = $this->performApiRequest('shorten', array('url' => $url));

    if (!empty($response['errors'])) {
      // Generic API error.
      throw new DlvrItApiResponseException('', key($response['errors']));
    }

    if (!isset($response[0]['short'])) {
      throw new DlvrItApiResponseException('API did not return a short URL.');
    }

    return $response[0]['short'];
  }

  /**
   * Get API key.
   *
   * @return string|null
   *   API key
   *
   * @throws DlvrItApiConfigurationException
   *   Missing configuration settings.
   */
  protected function getApiKey() {
    $key = isset($this->apiKey) ? $this->apiKey : variable_get('dlvrit_api_key', '');
    if (!$key) {
      throw new DlvrItApiConfigurationException('Missing API key.');
    }

    return $key;
  }

  /**
   * Override client API key.
   *
   * @param string|null $api_key
   *   API key or NULL to use global settings.
   */
  protected function setApiKey($api_key) {
    $this->apiKey = $api_key;
  }

  /**
   * Perform API request.
   *
   * @param string $method
   *   API method.
   * @param array $params
   *   Request parameters.
   *
   * @return array
   *   Response array.
   *
   * @throws DlvrItApiTransportException
   *   HTTP transport layer error.
   *
   * @throws DlvrItApiResponseException
   *   Unexpected API response error.
   */
  protected function performApiRequest($method, array $params = array()) {
    $defaults = array(
      'key' => $this->getApiKey(),
    );
    $params += $defaults;

    $boundary = md5(uniqid());
    $options = array(
      'headers' => array('Content-Type' => "multipart/form-data; boundary=$boundary"),
      'method' => 'POST',
      'data' => static::encodeMultipartData($boundary, $params),
    );

    $result = drupal_http_request($this->getRequestUrl($method), $options);

    if (isset($result->error)) {
      throw new DlvrItApiTransportException($result->error, $result->code);
    }

    $json = $result->data;
    $response = @drupal_json_decode($json);
    if ($response === NULL) {
      throw new DlvrItApiTransportException('Error parsing response JSON.');
    }

    return $response;
  }

  /**
   * Get API request URL.
   *
   * @param string $method
   *   API method.
   *
   * @return string
   *   API request URL.
   */
  protected function getRequestUrl($method) {
    return 'https://api.dlvrit.com/1/' . $method . '.json';
  }

  /**
   * Encode multipart form data.
   *
   * @param string $boundary
   *   Multipart form boundary.
   * @param array $params
   *   POST parameters.
   *
   * @return string
   *   Encoded POST.
   */
  protected static function encodeMultipartData($boundary, array $params) {
    $output = '';

    foreach ($params as $key => $value) {
      $output .= "--$boundary\r\n";
      if (is_object($value) && $value->type == 'file') {
        $output .= static::multipartEncFile($key, $value->path, $value->mime_type);
      }
      else {
        $output .= static::multipartEncText($key, $value);
      }
    }
    $output .= "--$boundary--";

    return $output;
  }

  /**
   * Encodes text field for HTTP POST request.
   *
   * @param string $name
   *   Field name.
   * @param string $value
   *   Field value.
   *
   * @return string
   *   Encoded form data.
   */
  protected static function multipartEncText($name, $value) {
    return "Content-Disposition: form-data; name=\"$name\"\r\n\r\n$value\r\n";
  }

  /**
   * Encodes file data for HTTP POST request.
   *
   * @param string $name
   *   Field name.
   * @param string $path
   *   File path.
   * @param string $mimetype
   *   MIME type.
   *
   * @return string
   *   Encoded form data.
   */
  protected static function multipartEncFile($name, $path, $mimetype) {
    if (substr($path, 0, 1) == "@") {
      $path = substr($path, 1);
    }
    $filename = basename($path);
    $data = "Content-Disposition: form-data; name=\"$name\"; filename=\"$filename\"\r\n";
    $data .= "Content-Transfer-Encoding: binary\r\n";
    $data .= "Content-Type: $mimetype\r\n\r\n";
    $data .= file_get_contents($path) . "\r\n";

    return $data;
  }

}

/**
 * Generic API client exception.
 */
class DlvrItApiException extends Exception {}

/**
 * API client configuration exception.
 */
class DlvrItApiConfigurationException extends DlvrItApiException {}

/**
 * API client transport exception.
 */
class DlvrItApiTransportException extends DlvrItApiException {}

/**
 * API client response exception.
 */
class DlvrItApiResponseException extends DlvrItApiException {

  /**
   * {@inheritdoc}
   */
  public function __construct($message = '', $code = 0, Throwable $previous = NULL) {
    if (!$message && $code) {
      $message = static::getErrorMessage($code);
    }
    parent::__construct($message, $code, $previous);
  }

  /**
   * Get dlvr.it API error message by code.
   *
   * @param int $code
   *   Error code.
   *
   * @return string
   *   Error message.
   *
   * @see https://api.dlvrit.com/1/help
   */
  protected static function getErrorMessage($code) {
    $codes = array(
      1 => t('Bad Method / Unexpected Error.'),
      2 => t('Database Failure. Try Again.'),
      3 => t('Missing / Invalid API Key.'),
      4 => t('Authentication Failure.'),
      5 => t('Transaction Limit Reached.'),
      6 => t('Permission Denied.'),
      101 => t('Missing / Invalid Route.'),
      102 => t('Missing / Invalid Account.'),
      103 => t('Missing / Invalid Message.'),
      104 => t('Missing / Invalid URL.'),
      105 => t('Invalid shorten account or URL.'),
      106 => t('Image too large or wrong type.'),
      999 => t('dlvr.it is currently offline.'),
      107 => t('Method only available to paid subscribers.'),
    );

    return isset($codes[$code]) ? $codes[$code] : t('Unknown error code @code', array('@code' => $code));
  }

}
